﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Globalization;



namespace parserJSON
{
    class Program
    {
        /// <summary>
        /// Удаление пробельных символов вне полей "value"
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DeleteWhiteSpace(string str)
        {
            bool flag_1 = true;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '"')
                    flag_1 = !flag_1;

                if (Char.IsWhiteSpace(str[i]) && flag_1)
                {
                    str = str.Remove(i, 1);
                    i--;
                }
            }
            return str;
        }

        public static void Main()
        {

            string text = System.IO.File.ReadAllText(@"..\\Debug\Example.txt", System.Text.Encoding.GetEncoding(1251)); // поддержка русского языка
            Json json = new Json(DeleteWhiteSpace(text)); // Формирование дерева Json

            json.PrintJSON();       // Обратная сборка дерева Json

            // json.PrintRecurs();  // Визуализация хранени данных в дереве Json

            Console.ReadKey();
        }
    }
}
