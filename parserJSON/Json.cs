﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parserJSON
{
    /// <summary>
    /// Тип текущего символа
    /// </summary>
    public enum Choice
    {
        QUOTE,      // Двойная ковычка
        BRACE,      // Фигурная скобка
        SQUARE,     // Квадратная скобка
        NUMBER,     // Цифра
        LETTER,     // Буква
        ERROR       // Ошибка
    }

    /// <summary>
    /// Тип значения-value узла json
    /// </summary>
    public enum TypeValue
    {
        PARENT,     // Родитель (возможен только в единственнм экземпляре)
        TREE,       // Дерево
        OBJECT,     // Объект {}
        ARRAY,      // Массив
        STRING,     // Строка
        NUMBER,     // Численное значение
        CONST       // true, false, null
    }

    /// <summary>
    /// Хранит индексы потомков в родительском dataChar[]
    /// </summary>
    public struct InfoChildsIndex
    {
        public int startIndex;
        public int finishIndex;
    }

    /// <summary>
    /// Дерево Json
    /// </summary>
    public class Json
    {
        //--------------------------------------------------------------------------------------
        private char[] dataChar;             // родительская строка
        private char[] nameCh;               //  Ключ
        private string name;                 //  Ключ
        private char[] valueCh;              //  Значение (строка, число, const, объект, массив)
        private string value;                //  Значение 
        private Json[] objNested;            // объект может содержать несколько потомков
        private InfoChildsIndex[] infoChild;                // Информация о возможных потомках 
        private TypeValue typeValue = TypeValue.STRING;     // тип значения value
        private int amountChild = 0;         // Кол-во потомков
        //--------------------------------------------------------------------------------------


        /// <summary>
        /// Конструктор, вызывается 1 раз (для "Parent")
        /// </summary>
        /// <param name="dataIn"></param>
        public Json(string dataIn)
        {
            dataChar = new char[dataIn.Length];
            dataChar = DeleteBraces(dataIn.ToCharArray());


            CountChild();
            char[] nameCh = {'j', 's', 'o', 'n'};

            typeValue = TypeValue.PARENT;

            if (CheckDataValidation())
            {
                if (amountChild == 0)
                {
                    amountChild = 1;
                    objNested = new Json[amountChild];
                    objNested[0] = new Json(dataChar, 0, dataChar.Length - 1);
                    return;
                }
                objNested = new Json[amountChild];
                for (int i = 0; i < amountChild; i++)
                    objNested[i] = new Json(dataChar, infoChild[i].startIndex, infoChild[i].finishIndex);
            }
            else
            {
                throw new ArgumentException("ERROR: CheckDataValidation() return false! or amountChild == 0");
            }
        }

        /// <summary>
        /// Рекурсивный констроктор JSON класса
        /// </summary>
        /// <param name="str"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public Json(char[] str, int left, int right)
        {
            int k = 0;
            dataChar = new char[right - left + 1];
            for (int i = left; i <= right; i++)
                dataChar[k++] = str[i];
            if (dataChar[0] == '{')
            {
                ReadObject(0);
                typeValue = TypeValue.OBJECT;
                char[] nameCh = {'E', 'M', 'P', 'T', 'Y'};
                return;
            }
            CountChild();
            if (amountChild > 1)
                typeValue = TypeValue.TREE;

            if (amountChild == 0)
            {
                int index = 1;
                ReadName(ref index);
                switch (ChoiceSwitch(dataChar[index]))
                {
                    case Choice.QUOTE: { index++; ReadValue(ref index, Choice.QUOTE); if (typeValue != TypeValue.TREE) typeValue = TypeValue.STRING; break; }
                    case Choice.NUMBER: { ReadValue(ref index, Choice.NUMBER); if (typeValue != TypeValue.TREE) typeValue = TypeValue.NUMBER; break; }
                    case Choice.LETTER: { ReadValue(ref index, Choice.LETTER); if (typeValue != TypeValue.TREE) typeValue = TypeValue.CONST; break; }
                    case Choice.BRACE: { ReadObject(index); if (typeValue != TypeValue.TREE) typeValue = TypeValue.OBJECT; return; }
                    case Choice.SQUARE: { ReadArray(ref index); if (typeValue != TypeValue.TREE) typeValue = TypeValue.OBJECT; break; } // заглушка
                    case Choice.ERROR: { Console.WriteLine("ERROR! (ParserREcurs) ChoiceSwitch return ERROR"); break; }
                }
            }

            objNested = new Json[amountChild];

            for (int i = 0; i < amountChild; i++)
                objNested[i] = new Json(dataChar, infoChild[i].startIndex, infoChild[i].finishIndex);
        }

        /// <summary>
        /// Визуализация одного "листа" JSON дерева
        /// </summary>
        public void Print()
        {
            if (typeValue != TypeValue.TREE && typeValue != TypeValue.PARENT)
            {
                Console.Write("\nName: ");
                foreach (var x in nameCh)
                    Console.Write(x);
            }

            if (typeValue != TypeValue.OBJECT && typeValue != TypeValue.PARENT && typeValue != TypeValue.ARRAY && typeValue != TypeValue.TREE)
            {
                Console.Write("\nValue: ");
                foreach (var x in valueCh)
                    Console.Write(x);
            }
            Console.Write("\ntypeValue: " + typeValue);
            if (amountChild != 0)
                Console.Write("\namountChild: " + amountChild);
            Console.WriteLine('\n');

        }


        /// <summary>
        /// Создание JSON файла для исходного дерева
        /// </summary>
        /// <param name="amountTab"></param>
        public void PrintJSON(int amountTab = 0)
        {
            switch (typeValue)
            {
                case TypeValue.PARENT:
                    {
                        Console.Write("{\n");
                        foreach (var x in objNested)
                            x.PrintJSON(amountTab + 1);
                        Console.Write("\n}");
                        break;
                    }
                case TypeValue.TREE:
                    {
                        foreach (var x in objNested)
                            x.PrintJSON(amountTab);
                        break;
                    }
                case TypeValue.OBJECT:
                    {
                        PrintChar('\t', amountTab);
                        PrintName();
                        Console.Write("{\n");
                        foreach (var x in objNested)
                            x.PrintJSON(amountTab + 1);
                        PrintChar('\t', amountTab);
                        Console.Write("},\n");
                        break;
                    }
                case TypeValue.STRING:
                    {
                        PrintChar('\t', amountTab);
                        Console.Write('"');
                        PrintName();
                        PrintValue();
                        Console.Write(",\n");
                        break;
                    }
                case TypeValue.NUMBER:
                    {
                        PrintChar('\t', amountTab);
                        Console.Write('"');
                        PrintName();
                        PrintValue();
                        Console.Write(",\n");
                        break;
                    }
                case TypeValue.CONST:
                    {
                        PrintChar('\t', amountTab);
                        Console.Write('"');
                        PrintName();
                        PrintValue();
                        Console.Write(",\n");
                        break;
                    }
            }
        }

        /// <summary>
        /// Печать символа в консоль
        /// </summary>
        /// <param name="x"></param>
        /// <param name="n"></param>
        public void PrintChar(char x, int n = 1)
        {
            for (int i = 0; i < n; i++)
                Console.Write(x);
        }

        public void PrintName()
        {
            if (nameCh.Length == 5 && nameCh[0] == 'E' && nameCh[1] == 'M' && nameCh[2] == 'P' && nameCh[3] == 'T' && nameCh[4] == 'Y')
                return;

            foreach (var x in nameCh)
                Console.Write(x);
            Console.Write("\": ");
        }

        public void PrintValue()
        {
            if (typeValue == TypeValue.STRING)
            {
                Console.Write('"');
                foreach (var x in valueCh)
                    Console.Write(x);
                Console.Write('"');
            }
            else
            {
                foreach (var x in valueCh)
                    Console.Write(x);
            }

        }

        /// <summary>
        /// Визуализация дерева хранения данных json класса
        /// </summary>
        public void PrintRecurs()
        {
            Print();
            for (int i = 0; i < amountChild; i++)
                objNested[i].PrintRecurs();
        }
        /// <summary>
        /// Определение токена
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private Choice ChoiceSwitch(char x)
        {
            if (x == '"')
                return Choice.QUOTE;
            if (x == '{')
                return Choice.BRACE;
            if (x == '[')
                return Choice.SQUARE;
            if (char.IsDigit(x))
                return Choice.NUMBER;
            if (char.IsLetter(x))
                return Choice.LETTER;
            return Choice.ERROR;
        }

        private void ReadName(ref int index)
        {
            int startIndex = index;
            int lengthName = 0;
            while (dataChar[index] != '"')
            {
                lengthName++;
                index++;
            }

            nameCh = new char[lengthName];

            for (int k = 0; k < lengthName; k++)
                nameCh[k] = dataChar[startIndex + k];

            index += 2;
            name = new string(nameCh);
        }

        private void ReadValue(ref int index, Choice choice)
        {
            int startIndex = index;
            int lengthValue = 0;
            if (choice == Choice.QUOTE) // для строки
            {
                while (dataChar[index] != '"')
                {
                    lengthValue++;
                    index++;
                }
                index++;
            }
            if (choice == Choice.NUMBER) // для числа
            {
                while (index != dataChar.Length && (Char.IsDigit(dataChar[index]) || dataChar[index] == '.'))
                {
                    lengthValue++;
                    index++;
                }
            }
            if (choice == Choice.LETTER) // для true, false, null ...
            {
                while (index != dataChar.Length && Char.IsLetter(dataChar[index]))
                {
                    lengthValue++;
                    index++;
                }
            }
            valueCh = new char[lengthValue];
            for (int k = 0; k < lengthValue; k++)
                valueCh[k] = dataChar[startIndex + k];
            value = new string(valueCh);
        }

        /// <summary>
        /// Заглушечная версия работы с json - массивом
        /// </summary>
        /// <param name="index"></param>
        private void ReadArray(ref int index)
        {
            throw new ArgumentException("ERROR! This version does not support working with arrays");
        }

        private void ReadObject(int index)
        {
            amountChild = 1;
            infoChild = new InfoChildsIndex[1];
            infoChild[0].startIndex = index + 1;
            infoChild[0].finishIndex = dataChar.Length - 2;
            objNested = new Json[amountChild];

            for (int i = 0; i < amountChild; i++)
                objNested[i] = new Json(dataChar, infoChild[i].startIndex, infoChild[i].finishIndex);

        }

        /// <summary>
        /// Сбор информации о потомках в dataChar
        /// </summary>
        private void CountChild()
        {
            int countBrace = 0;
            bool outQuote = true;

            for (int j = 0; j < dataChar.Length; j++) // убрать внутренние запятые
            {
                if (dataChar[j] == '"')
                    outQuote = !outQuote;
                if (dataChar[j] == '{')
                    countBrace++;
                if (dataChar[j] == '}')
                    countBrace--;
                if (dataChar[j] == ',' && countBrace == 0 && outQuote)
                {
                    if (amountChild == 0)
                        amountChild++;
                    amountChild++;
                }
            }
            if (amountChild == 0)
                return;

            infoChild = new InfoChildsIndex[amountChild];
            int i = 0;

            infoChild[0].startIndex = i;
            outQuote = true;
            for (int k = 1; i < dataChar.Length; i++) // FIX вложенность запятых в другие объекты
            {
                if (dataChar[i] == '"')
                    outQuote = !outQuote;
                if (dataChar[i] == '{')
                    countBrace++;
                if (dataChar[i] == '}')
                    countBrace--;
                if (dataChar[i] == ',' && countBrace == 0 && outQuote)
                {
                    infoChild[k - 1].finishIndex = i - 1;
                    infoChild[k].startIndex = i + 1;
                    k++;
                }
            }
            infoChild[amountChild - 1].finishIndex = dataChar.Length - 1;
        }


        /// <summary>
        /// Заглушечная версия проверки на корректность входных данных
        /// </summary>
        /// <returns></returns>
        public bool CheckDataValidation()
        {
            return true;
            ////Проверки
            //if (dataChar[0] != '{' || dataChar[dataChar.Length - 1] != '}')
            //    return false;

            //int count1 = 0;
            //int count2 = 0;
            //int count3 = 0;
            //foreach (var x in dataChar)
            //{
            //    switch (x)
            //    {
            //        case '{': { count1++; break; };
            //        case '}': { count1--; break; };
            //        case '[': { count2++; break; };
            //        case ']': { count2--; break; };
            //        case '"': { count3++; break; };
            //    }
            //}

            //return (count1 == 0 && count2 == 0 && count3 % 2 == 0);
        }

        /// <summary>
        /// Удаление парных фигурных скобок
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public char[] DeleteBraces(char[] str)
        {
            if (str[0] != '{' && str[str.Length - 1] != '}')
                return str;
            char[] res = new char[str.Length - 2];
            for (int i = 0; i < res.Length; i++)
                res[i] = str[i + 1];
            return res;
        }
    }
}
